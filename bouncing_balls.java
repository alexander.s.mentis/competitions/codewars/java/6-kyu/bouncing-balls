public class BouncingBall {
	
	public static int bouncingBall(double h, double bounce, double window) {
	    
      // initial conditions check
      if (h <= 0 || bounce <= 0 || bounce >= 1 || window >= h) {
          return -1;
      }
    
      int count = 1; // initial drop seen on the way down (window must be less than h)
      do {
          // we see the ball twice on every bounce that passes the window
          // once on the way up and once on the way down
          h *= bounce;
          if (h > window) {
              count += 2;
          }          
      } while (h > window);
    
      return count;
	}
}